class ChangeMatchPointToMatchValidateDetails < ActiveRecord::Migration[5.1]
  def change
    change_column :match_validate_details, :match_point, :integer, limit: nil
  end
end
