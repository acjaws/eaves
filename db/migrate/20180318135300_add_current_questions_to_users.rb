class AddCurrentQuestionsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :current_questions, :text
  end
end
