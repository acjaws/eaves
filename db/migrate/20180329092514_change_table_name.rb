class ChangeTableName < ActiveRecord::Migration[5.1]
  def change
    rename_column :match_validate_details, :comment, :content
    rename_column :match_validate_details, :match_point, :score
    rename_table :match_validate_details, :comments
    rename_column :match_validates, :details_count, :comments_count
    rename_table :match_validates, :verifies
    rename_column :comments, :match_validate_id, :verify_id
  end
end
