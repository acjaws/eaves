class AddMatchStatusOnMatch < ActiveRecord::Migration[5.1]
  def change
        add_column :matches, :match_status, :string, :default => 'WAITING'
  end
end
