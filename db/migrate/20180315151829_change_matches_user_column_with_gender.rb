class ChangeMatchesUserColumnWithGender < ActiveRecord::Migration[5.1]
  def change
    rename_column :matches, :user_1_id, :male_user_id
    rename_column :matches, :user_2_id, :female_user_id
  end
end
