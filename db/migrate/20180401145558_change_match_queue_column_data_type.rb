class ChangeMatchQueueColumnDataType < ActiveRecord::Migration[5.1]
  def change
    change_column :match_queues, :male_queue, :text
    change_column :match_queues, :female_queue, :text
  end
end
