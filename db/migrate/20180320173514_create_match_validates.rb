class CreateMatchValidates < ActiveRecord::Migration[5.1]
  def change
    create_table :match_validates do |t|
      t.integer :match_id
    end     
      
    add_column :match_validates, :question_ids, :text
    add_column :match_validates, :male_answers_ids, :text
    add_column :match_validates, :female_answers_ids, :text
              
  end
end
