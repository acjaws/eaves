class ChangeDeaultAutoEnvelopeInUsers < ActiveRecord::Migration[5.1]
  def change
    change_column :users, :auto_envelope, :boolean, default: true
  end
end
