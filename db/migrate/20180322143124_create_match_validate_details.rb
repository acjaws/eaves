class CreateMatchValidateDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :match_validate_details do |t|
      t.integer :match_validate_id, null: false
      t.integer :user_id, null: false
      t.integer :match_point
      t.text :comment

      t.timestamps
    end
  end
end
