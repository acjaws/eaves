class ChangeMatchStatusNameToMatches < ActiveRecord::Migration[5.1]
  def change
    rename_column :matches, :match_status, :status
  end
end
