class AddColumnToVerifies < ActiveRecord::Migration[5.1]
  def change
    add_column :verifies, :completed_comments_count, :integer, default: 0
  end
end
