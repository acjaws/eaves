class AddDetailsCountToMatchValidates < ActiveRecord::Migration[5.1]
  def change
    add_column :match_validates, :details_count, :integer, default: 0    
  end
end
