class ChangeColumnToMatchValidateDetails < ActiveRecord::Migration[5.1]
  def change
    change_column :match_validate_details, :match_point, :integer, null: true
    change_column :match_validate_details, :comment, :text, null: true
  end
end
