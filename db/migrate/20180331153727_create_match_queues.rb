class CreateMatchQueues < ActiveRecord::Migration[5.1]
  def change
    create_table :match_queues do |t|
      t.integer :male_queue
      t.integer :female_queue

      t.timestamps
    end
  end
end
