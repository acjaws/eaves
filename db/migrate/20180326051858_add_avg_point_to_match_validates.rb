class AddAvgPointToMatchValidates < ActiveRecord::Migration[5.1]
  def change
    add_column :match_validates, :avg_points, :integer, default: 0
  end
end
