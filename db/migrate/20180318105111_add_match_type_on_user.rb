class AddMatchTypeOnUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :match_type, :string, :default => 'MATCH_FORBID'
  end
end
