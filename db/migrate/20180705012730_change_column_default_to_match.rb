class ChangeColumnDefaultToMatch < ActiveRecord::Migration[5.1]
  def change
    change_column :matches, :status, :string, default: "ACTIVE"
  end
end
