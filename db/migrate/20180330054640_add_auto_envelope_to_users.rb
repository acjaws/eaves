class AddAutoEnvelopeToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :auto_envelope, :boolean, :default => 1
  end
end
