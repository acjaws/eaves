class RenameAvgPointsToAvg < ActiveRecord::Migration[5.1]
  def change
    rename_column :verifies, :avg_points, :avg_score
  end
end
