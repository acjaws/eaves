class AddColumnToMatch < ActiveRecord::Migration[5.1]
  def change
    add_column :matches, :twice, :boolean, default: false
  end
end
