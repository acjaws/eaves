class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
    	t.string :title
    	t.text :female_content
    	t.text :male_content
    	t.string :category
    	t.float :weight

      t.timestamps
    end
  end
end
