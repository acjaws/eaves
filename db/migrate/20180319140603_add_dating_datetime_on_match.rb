class AddDatingDatetimeOnMatch < ActiveRecord::Migration[5.1]
  def change
    add_column :matches, :dating_datetime, :datetime, null: true
  end
end
