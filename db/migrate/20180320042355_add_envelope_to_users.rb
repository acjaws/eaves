class AddEnvelopeToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :envelope, :integer, default: 0
  end
end
