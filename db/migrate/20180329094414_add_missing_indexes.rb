class AddMissingIndexes < ActiveRecord::Migration[5.1]
  def change
    add_index :answers, :question_id
    add_index :answers, :user_id
    add_index :answers, [:question_id, :user_id]
    add_index :matches, :female_user_id
    add_index :matches, :male_user_id
  end
end