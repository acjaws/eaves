class MatchExpiredJob < ApplicationJob
  queue_as :expired

  def perform(id)
    match = Match.find(id)
    match.change_status_to_expired
  end

end