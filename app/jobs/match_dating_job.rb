class MatchDatingJob < ApplicationJob
  queue_as :dating

  def perform(id)
    match = Match.find(id)
    match.change_status_to_dating
  end

end
