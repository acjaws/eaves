class Admin::MatchesController < Admin::BaseController

  before_action :set_match, only: [:show]
  def index
    @ransack  = Match.includes(:male_user, :female_user).ransack(params[:q])
    @matches = @ransack.result(distinct: true).page(params[:page]).per(20)
    @match_count = Match.count
    @status = {'WAITING' => '等待中', 'ACTIVE' => '約會中', 'EXPIRED' => '約會過期'}
  end

  def show
  end

  private

  def set_match
    @match = Match.find(params[:id])
  end

end
