class Admin::AnswersController < Admin::BaseController
  def index
    @ransack = Answer.includes(:question, :user).ransack(params[:q])
    @answers = @ransack.result(distinct: true).page(params[:page]).per(20)
    @answer_count = Answer.count
  end
end
