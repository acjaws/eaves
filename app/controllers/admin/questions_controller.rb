class Admin::QuestionsController < Admin::BaseController

  before_action :set_question, only: [:edit, :update, :show]

  def index
    @ransack = Question.all.ransack(params[:q])
    @questions = @ransack.result(distinct: true).page(params[:page]).per(20)
    @question_count = Question.count
  end

  def edit
  end

  def show
  end

  def update
    @question.update(question_params)
    flash[:notice] = '編輯成功！'
    redirect_to admin_questions_path
  end

  private

  def set_question
    @question = Question.find(params[:id])
  end

  def question_params
    params.require(:question).permit(:title, :male_content, :female_content, :category)
  end

end
