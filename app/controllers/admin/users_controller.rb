class Admin::UsersController < Admin::BaseController

  before_action :set_user, only: [:show]

  def index
    @ransack = User.ransack(params[:q])
    @users = @ransack.result(distinct: true).page(params[:page]).per(20)
    @user_count = User.count
  end

  def show
  end


  private

  def set_user
    @user = User.find(params[:id])
  end

end
