class CommentsController < ApplicationController
  def index
    @match = Match.find(params[:match_id])
    if @match.male_user == current_user || @match.female_user == current_user
      if @match.status == "EXPIRED"
        @comments = @match.verify.comments.all
      else
        redirect_to my_matches_path
      end
    else
      @comments = @match.verify.comments.all
    end
  end
end
