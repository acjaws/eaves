class ConversationsController < ApplicationController

  def index
    if current_user.gender == "Male"
      match_user_ids = Match.where(male_user: current_user, twice: true, status: "EXPIRED").pluck(:female_user_id).uniq
    else
      match_user_ids = Match.where(female_user: current_user, twice: true, status: "EXPIRED").pluck(:male_user_id).uniq
    end
    @users = User.where(id: match_user_ids)
    @conversations = Conversation.all
  end

  def create
    if Conversation.between(params[:sender_id],params[:recipient_id]).present?
      @conversation = Conversation.between(params[:sender_id],params[:recipient_id]).first
    else
      @conversation = Conversation.create!(conversation_params)
    end
    redirect_to conversation_messages_path(@conversation)
  end

  private

  def conversation_params
    params.permit(:sender_id, :recipient_id)
  end
end
