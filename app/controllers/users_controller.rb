class UsersController < BaseController
  before_action :set_user, only: [:index, :edit, :update, :exchange_envelope]
  before_action :user_info_completed?, except: [:edit, :update, :emailcheck]
  skip_before_action :authenticate_user!, only: [:emailcheck]

  def show
    @user = User.find(params[:id])
    if @user != current_user
      set_user
      redirect_to user_path(current_user), alert: "無法偷看別人喔！"
    end
  end

  def update
    if @user.username
      if @user.update(user_params)
        flash[:notice] = "更新成功"
        redirect_to user_path(@user)
      else
        flash[:alert] = "更新失敗，請填完所有欄位"
        render :edit
      end
    else
      if @user.update(user_params)
        flash[:notice] = "Hi #{current_user.username}，歡迎加入Eaves "
        redirect_to root_path
      else
        flash[:alert] = "更新失敗，請填完所有欄位"
        render :edit
      end
    end
  end

  def edit
    @user = User.find(params[:id])
    if @user != current_user
      set_user
      redirect_to edit_user_path(current_user), alert: "無法偷看別人喔！"
    end
  end

  def exchange_envelope
    if @user.exchange_envelope
    else
      flash[:alert] = "分數不足"
    end
    redirect_back(fallback_location: root_path)
  end

  def emailcheck
  #https://stackoverflow.com/questions/31579967/ajax-check-if-email-already-exist-ruby-on-rails
    @user = User.where(email:params[:email])
    puts(@user)
    respond_to do |format|
      format.json {render :json => {email_exists: @user.present?}} #sir Deep suggestion to return true or false for email_exists or the code below
     # format.json {render :json => @user} #this will output null if email is not in the database
    end
  end

  private

  def user_params
    params.require(:user).permit(:username, :gender, :birthday, :occupation, :salary, :interest, :auto_envelope)
  end

  def set_user
    @user = current_user
  end

end
