class BaseController < ApplicationController
  before_action :user_info_completed?

  private

  # 檢查使用者資料是否填完整
  def user_info_completed?
    unless current_user.basic_info_completed?
      flash[:alert] = "請填寫完整使用者資料"
      redirect_to edit_user_path(current_user)
    end
  end
end