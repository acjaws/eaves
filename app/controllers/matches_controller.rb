class MatchesController < BaseController
  skip_before_action :verify_authenticity_token
  before_action :set_match, only: [:show, :verify, :set_qnagroup]
  before_action :set_qnagroup, only: [:show, :verify]
  after_action :save_my_previous_url, only: [:show]

  #以下新增，邏輯待修正
  def my
    @my_match_numbers = Match.where(male_user_id: current_user.id).or(Match.where(female_user_id: current_user.id)).includes(:male_user, :female_user, :verify).length
    @recent_matches = Match.includes(:male_user, :female_user, :verify)
                        .where.not(male_user_id: current_user.id)
                        .where.not(female_user_id: current_user.id)
                        .order(updated_at: :desc).limit(5)
    @success_matches = Match.expired.where(male_user_id: current_user.id).or(Match.where(female_user_id: current_user.id))
                        .includes(:male_user, :female_user, :verify)
                        .joins(:verify).merge(Verify.where("avg_score >= ?", 60 ))
  end

  def my_all
    @matches = Match.where(male_user_id: current_user.id).or(Match.where(female_user_id: current_user.id)).includes(:male_user, :female_user, :verify)
  end

  def help
    @my_help_matches = Match.where(id: current_user.verifies.pluck(:match_id)).active
    @need_help_matches = Match.active.includes(:male_user, :female_user, :verify)
                .where.not(male_user_id: current_user.id)
                .where.not(female_user_id: current_user.id)
                .joins(:verify).merge(Verify.order("comments_count ASC")) - @my_help_matches
    @matches_count = @my_help_matches.size + @need_help_matches.size
    @need_help_matches = @need_help_matches[0..8]
  end

  def help_all
    @matches = Match.active.includes(:male_user, :female_user, :verify)
                .where.not(male_user_id: current_user.id)
                .where.not(female_user_id: current_user.id)
                .order("created_at ASC")
  end




  #以下沒動
  def index
    #以約會時間快到期與驗證人數未滿篩選出精選約會pick_matches
    @matches = Match.includes(:male_user, :female_user, :verify).where.not(male_user_id: current_user.id).where.not(female_user_id: current_user.id).where(status: "ACTIVE").order("dating_datetime ASC")
               .joins(:verify).merge(Verify.order("comments_count ASC"))
    @pick_matches = @matches.joins(:verify).merge(Verify.where.not(comments_count: 5)).limit(5)
    #挑出約會
    if current_user.gender == "Male"
      @expired_datings = Match.where(male_user: current_user, status: "EXPIRED")
      @recent_datings = @expired_datings.limit(5)
    elsif current_user.gender == "Female"
      @expired_datings = Match.where(female_user: current_user, status: "EXPIRED")
      @recent_datings = @expired_datings.limit(5)
    end

    @status = current_user.check_answered_all_question

    #功能窗格顯示資料使用
    @dated_all_len = @expired_datings.length
    if current_user.gender == "Male"
      @dated_good_len = Match.where(male_user: current_user, status: "EXPIRED").joins(:verify).merge(Verify.where("avg_score >= ?",60)).length
    else
      @dated_good_len = Match.where(female_user: current_user, status: "EXPIRED").joins(:verify).merge(Verify.where("avg_score >= ?",60)).length
    end
    @answered_all_len = Answer.where(user: current_user).length
    if current_user.match_type == "MATCH_FORBID"
      @unanswered_len = Question.where(category: "basic").length-current_user.answered_questions.where(category: "basic").length
    else
      @unanswered_len = current_user.current_questions.length
    end

    #未完成評論連結
    unfinished_comment = current_user.comments.where(content: nil).or(current_user.comments.where(score: nil)).or(current_user.comments.where(content: "")).first
    if unfinished_comment
      @unfinished_match = unfinished_comment.verify.match
    end

  end

  def show
    @other_comments = @match.verify.comments.where.not(user_id: current_user.id, content: nil)
    @back_url = session[:my_previous_url]
    # 檢查此配對是否為約會狀態
    if @match.status != "ACTIVE"
      flash[:alert] = "約會尚未開始或已結束"
      redirect_back(fallback_location: root_path)
      #檢查使用者是否進到自己的評論
    elsif @male_user.id == current_user.id || @female_user.id == current_user.id
      flash[:alert] = "不可評論自己的約會"
      redirect_back(fallback_location: root_path)
        #檢查是否已經評論完畢或者已經加入評論清單，把該筆評論抓出來丟給前端顯示
    elsif current_user.join_comment?(@match)
      @comment = @match.verify.comments.find_by(user_id: current_user.id)
    elsif @match.verify.comments_count == 5
      flash[:alert] = "此約會驗證已滿"
      redirect_back(fallback_location: help_matches_path)
    elsif Match.where(id: current_user.verifies.where("completed_comments_count < ?", 5).pluck(:match_id)).active.size < 5
      @comment = @match.verify.comments.build(user_id: current_user.id)
      @comment.save( validate: false )
    else
      flash[:alert] = "評論約會數量達最大值，請等候其他網友完成評論"
      redirect_to help_matches_path
    end
  end

  def verify
    @comment = current_user.comments.find_by(verify_id: @match.verify.id)
    if @comment.comment_completed?
      if @comment.update(comment_params)
        flash[:notice] = "更新評論"
        redirect_to help_matches_path
      else
        flash.now[:alert] = "內容未填完喔"
        @back_url = session[:my_previous_url]
        @comment = @match.verify.comments.build(comment_params)
        render :show
      end
    else
      if @comment.update(comment_params)
        current_user.add_points 150
        Notification.create(notify_type: 'add_points', user: current_user, content: "#{current_user.points}")
        flash[:notice] = "更新評論"
        redirect_to help_matches_path
      else
        flash.now[:alert] = "內容未填完喔"
        @back_url = session[:my_previous_url]
        @comment = @match.verify.comments.build(comment_params)
        render :show
      end
    end
  end

  def getAnswersHistory
    @user_id = params[:user_id]
    @answers = Answer.where(user_id: @user_id)
    @user = User.find(@user_id)
    @questions = []
    @answers.each do |answer|
      @questions.push(answer.question.title)
    end
    render :json => [@answers, @questions, @user]
  end

  def getBasicQuestion

    @empty_responses = []
    exclude_questions = []
    #取出current_user 所回答的basic類別問題
    current_user.answered_questions.where(category: "basic").each do |q|
      exclude_questions << q.id
    end
    @questions = Question.where(category: "basic").where.not(id: exclude_questions)
    #再用where.not 取出current_user還沒回答過的基本問題

    @gender = User.find(params[:user_id]).gender
    @user_id = params[:user_id]

    render :json => [@questions, @gender, @user_id]

  end

  def postBasicQuestion
    @user_id = params[:user_id]
    @answers = params[:answers]
    @question_ids = params[:question_ids]

    for i in 0...@answers.length
      answer = Answer.new
      answer.user_id = @user_id
      answer.question_id = @question_ids[i]
      answer.content = @answers[i]
      answer.save
    end
  end

   def getAdvancedQuestion

    @empty_responses = []

    def put_in_questions
      if @questions.exists?
        @gender = User.find(params[:user_id]).gender
        @user_id = params[:user_id]
        render :json => ['HAS_QUESTION', @questions, @gender, @user_id]
      else
        render :json => ['NO_NEW_QUESTION']
      end
    end

    if current_user.match_type != "MATCH_FORBID"
    # MATCH FORBID狀態代表未答完基本問題
      @questions = Question.where(id: current_user.current_questions)
      put_in_questions
    else
     render :json => ['BASIC_QUESTION_FIRST']
    end
  end

  def postAdvancedQuestion
    @user_id = params[:user_id]
    @answers = params[:answers]
    @question_ids = params[:question_ids]

    for i in 0...@answers.length
      answer = Answer.new
      answer.user_id = @user_id
      answer.question_id = @question_ids[i]
      answer.content = @answers[i]
      answer.save

      @user = User.find(@user_id)

      @user.current_questions.delete(@question_ids[i].to_i)
      @user.save!

    end
  end

  def postAutoEnvelope
    @user = User.find(params[:user_id])
    @auto_envelope = params[:auto_envelope]
    @user.auto_envelope = @auto_envelope
    @user.save
  end


  private

  def save_my_previous_url
    session[:my_previous_url] = URI(request.referer || '').path
  end

  def set_qnagroup
    # 把Ｑ和Ａ存成ＡＲＲＡＹ再吐給前端處裡
    @qna_groups = []
    @male_user = @match.male_user
    @female_user = @match.female_user
    @questions = Question.where(id: @match.verify.question_ids).order(id: :asc)
    @male_answers = Answer.where(id: @match.verify.male_answers_ids).order(question_id: :asc).pluck(:content)
    @female_answers = Answer.where(id: @match.verify.female_answers_ids).order(question_id: :asc).pluck(:content)
    @questions.each_with_index do |question,index|
      @qna_groups << { question_id: question.id,
                       male_content: question.male_content,
                       female_content: question.female_content,
                       male_answer: @male_answers[index],
                       female_answer: @female_answers[index]
                     }
    end
  end

  def set_match
    @match = Match.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:score, :content)
  end
end
