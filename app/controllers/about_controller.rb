class AboutController < ApplicationController
	skip_before_action :authenticate_user!, only: [:index, :team, :consent]

  def index
  end

  def team
  end

  def consent
  end

end
