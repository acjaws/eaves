class PagesController < BaseController

  #以下新增，邏輯待修正

  def me
    @answer = Answer.new
    exclude_questions = []
    current_user.answered_questions.each do |q|
      exclude_questions << q.id
    end

    @first_category_questions = Question.where(category: Question::QUESTION_CATEGORY[0]).where.not(id: exclude_questions)
    @second_category_questions = Question.where(category: Question::QUESTION_CATEGORY[1]).where.not(id: exclude_questions)
    @third_category_questions = Question.where(category: Question::QUESTION_CATEGORY[2]).where.not(id: exclude_questions)
    @answers_count = current_user.answered_questions.size
  end

  def me_done
    @questions = current_user.answered_questions
    @me_answers_0 = current_user.answers.joins(:question).merge(Question.where(category: Question::QUESTION_CATEGORY[0]))
    @me_answers_1 = current_user.answers.joins(:question).merge(Question.where(category: Question::QUESTION_CATEGORY[1]))
    @me_answers_2 = current_user.answers.joins(:question).merge(Question.where(category: Question::QUESTION_CATEGORY[2]))
  end

  def answer
    @answer = Answer.new(my_response_params)
    @answer.user_id = current_user.id
    if @answer.save!
      current_user.check_answered_question
      flash[:notice] = "提交成功"
      redirect_to me_pages_path
    else
      flash[:alert] = "提交失敗"
      redirect_to me_pages_path
    end
  end

  #等下面整理好再擺進private
  def my_response_params
    params.permit(:question_id, :content)
  end


  #以下舊的，沒動

  # 敘述: 目的是建立一個通用的形式，方便未來可以自由地傳遞任意個問題到form
  # 以陣列的方式導入多個Answer.new實例變數，再交由form以多層結構形式傳回params
  # "responses"=>
  # {
  #   "0"=>{"content"=>"this is me", "user_id"=>"23", "question_id"=>"1"},
  #   "1"=>{"content"=>"this is you", "user_id"=>"23", "question_id"=>"2"},
  #   "2"=>{"content"=>"this is she", "user_id"=>"23", "question_id"=>"3"}
  # }
  # 目前還沒有寫避免空白，之後再以JS處理

  def index
    @empty_responses = []
    exclude_questions = []
    #取出current_user 所回答的basic類別問題
    current_user.answered_questions.where(category: "basic").each do |q|
      exclude_questions << q.id
    end
    @questions = Question.where(category: "basic").where.not(id: exclude_questions)
    #再用where.not 取出current_user還沒回答過的基本問題
    if @questions.exists?
      puts @questions.class
      @title = "回答基本問題"
      @questions.each do
        @empty_responses << Answer.new
      end
    else
      redirect_to root_path
      flash[:alert] = "沒有新的問題了"
    end
  end

  #先用new作回答進階問題的controller
  #如果已經回答過基本問題
    #再判斷current_questions是否為空(in finction: put_in_questions)
      #如果是，沒有問題->就返回首頁
      #如果否，就從current_quesiotns的問題繼續作答
  #如果基本問題還沒回答，導回首頁

  def new
    @empty_responses = []

    def put_in_questions
      if @questions.exists?
        @title = "回答進階問題"
        @questions.each do
          @empty_responses << Answer.new
        end
      else
        redirect_to root_path  #rails 似乎不允許controller出現多個redirect_to，故用function包起來
        flash[:alert] = "沒有新的問題了"
      end
    end

    if current_user.answered_questions.where(category: "basic").distinct.pluck(:question_id).sort.length >= Question.where(category: "basic").length
    #已經完整回答過基本問題的判斷式
      @questions = Question.where(id: current_user.current_questions)
      put_in_questions
    else
      redirect_to root_path
      flash[:alert] = "請先回答完基本問題"
    end
  end

  def create
    params["responses"].each do |key, value|
      Answer.create(response_params(value))

      if not value["content"].empty?  #用於回答進階問題時的判斷(回答完的題目移出current_questions)，回答基本問題時不作動
        current_user.current_questions.delete(value["question_id"].to_i)
        current_user.save
      end
    end
    flash[:notice] = "提交成功"
    redirect_to root_path
  end

  private

  def response_params(my_params)
    my_params.permit(:question_id, :user_id, :content, :current_questions)
  end
end
