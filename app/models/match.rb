# == Schema Information
#
# Table name: matches
#
#  id              :integer          not null, primary key
#  male_user_id    :integer
#  female_user_id  :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  dating_datetime :datetime
#  status          :string           default("ACTIVE")
#  twice           :boolean          default(FALSE)
#

class Match < ApplicationRecord
  # 允許重複配對
  # validates :male_user_id, uniqueness: { scope: :female_user_id }

  belongs_to :male_user, :class_name => "User", :foreign_key => 'male_user_id'
  belongs_to :female_user, :class_name => "User", :foreign_key => 'female_user_id'
  has_one :verify

  # after_create :set_job_for_dating_and_expire
  after_create :add_new_verify
  # after_create :set_user_type_to_matched
  after_create :set_user_type_to_dating
  after_update :set_user_type_to_allow

  scope :active, -> { where(status: "ACTIVE") }
  scope :expired, -> { where(status: "EXPIRED") }

  # 檢查目前配對狀態並切換到下一個狀態
  def change_status_to_dating
    if self.status == "WAITING"
      self.update(status: "ACTIVE")
    end
  end

  def change_status_to_expired
    if self.status == "ACTIVE"
      self.update(status: "EXPIRED")
      # 檢查是否有尚未評論完成的，強制結束驗證
      comments = self.verify.comments
      if comments.present?
        comments.each do |comment|
          unless comment.comment_completed?
            comment.destroy
          end
        end
      end
      # 檢查約會是否及格
      if self.verify.avg_score >= 60
        # 集合約會檢查是否第二次
        if self.twice
          Notification.create(notify_type: 'noti', user: self.male_user, content:"恭喜找到推薦的另一半，#{self.female_user.username}，傳個訊息聊聊吧！", link: "conversations")
          Notification.create(notify_type: 'noti', user: self.female_user, content:"恭喜找到推薦的另一半，#{self.male_user.username}，傳個訊息聊聊吧！", link: "conversations")
          self.male_user.add_badge(1)
          self.female_user.add_badge(1)
        else
          Notification.create(notify_type: 'add_result', target: self, second_target: self.verify, user: self.male_user)
          Notification.create(notify_type: 'add_result', target: self, second_target: self.verify, user: self.female_user)
          # 第一次及格送第二次約會
          self.dating_again(true)
        end
      else
        # 約會沒及格檢查是否完全沒人評論，無人評論則送稍後在次約會並通知
        if self.verify.comments_count == 0
          Notification.create(notify_type: 'noti', user: self.male_user, content:"很可惜這次約會無人參與驗證，系統將重新安排！")
          Notification.create(notify_type: 'noti', user: self.female_user, content:"很可惜這次約會無人參與驗證，系統將重新安排！")
          if self.twice
            self.dating_again(self.twice)
          else
            self.dating_again(self.twice)
          end
        else
          Notification.create(notify_type: 'add_result', target: self, second_target: self.verify, user: self.male_user)
          Notification.create(notify_type: 'add_result', target: self, second_target: self.verify, user: self.female_user)
        end
      end
    end
  end

  # 第二次約會
  def dating_again(twice)
    male_user = self.male_user
    female_user = self.female_user
    dating_datetime = rand(5.hours).seconds.since.to_datetime
    Match.create(male_user: male_user, female_user: female_user, dating_datetime: dating_datetime, twice: twice)
  end

  # 找是否有重複配對
  def has_duplicate?
    # Match.group(:male_user_id,:female_user_id).having("count(*) > 1").present?
    self.male_user.female_pairings.count > 1
  end

  # 找出之前相同配對已經使用過的答案
  def used_questions
    matches = Match.where(male_user: self.male_user, female_user: self.female_user)
    @used_q_id = []
    matches.each do |match|
      if match.verify.present?
        puts match.verify.question_ids
        @used_q_id << match.verify.question_ids
      end
    end
    return @used_q_id
  end

  private

  # 設定回呼，每一筆配對成立後會送排成約會開始時間和結束時間
  # 取消以時間設定約會是否開啟-20180705
  # def set_job_for_dating_and_expire
  #   # 約會時間到達變更狀態排程
  #   active_time = self.dating_datetime
  #   expired_time = self.dating_datetime + 3.hours
  #   MatchDatingJob.set(queue: :dating, wait_until: active_time).perform_later(self.id)
  #   # 約會時間結束變更狀態排程
  #   MatchExpiredJob.set(queue: :expired, wait_until: expired_time).perform_later(self.id)
  # end

  # 設定回呼，每一筆配對成立後呼叫user的切換狀態到配對中
  def set_user_type_to_matched
    self.male_user.change_type_to_matched
    self.female_user.change_type_to_matched
  end

  # 設定回呼，每一筆配對更新後檢查配對為約會則呼叫user的切換狀態到約會中
  def set_user_type_to_dating
    if self.status == "ACTIVE"
      self.male_user.change_type_to_dating
      self.female_user.change_type_to_dating
    end
  end

  # 設定回呼，每一筆配對更新後檢查配對為已結束則呼叫user的切換狀態到等待配對
  def set_user_type_to_allow
    if self.status == "EXPIRED"
      self.male_user.change_type_to_allow
      self.female_user.change_type_to_allow
    end
  end

  # 設定回呼，每一筆配對成立後產生驗證表單，儲存雙方答案
  def add_new_verify
    # 抓出雙方各自回答過問題的id
    @male_user_answered_questions = self.male_user.answered_questions.ids
    @female_user_answered_questions = self.female_user.answered_questions.ids
    # 檢查是否有重複配對
    if self.has_duplicate?
      # 拿出前面配對已經有被驗證過的題目
      used_questions = self.used_questions
      # 找出有同時回答過的問題
      @same_questions = (@male_user_answered_questions & @female_user_answered_questions)
      # 排除已經有被驗證過的題目
      used_questions.each do |q|
        @same_questions.keep_if {|id| id != q }
      end
      # 如果剩下的題目少於三題，就允許全部題目隨機挑
      if @same_questions.length < 3
        @same_questions = (@male_user_answered_questions & @female_user_answered_questions)
      else
        @same_questions = @same_questions.sample(3)
      end
    else
      # 找出有同時回答過的問題
      @same_questions = (@male_user_answered_questions & @female_user_answered_questions).sample(3)
    end

    # 建立驗證record
    question_ids = []
    male_answers_ids = []
    female_answers_ids = []
    @same_questions.each do |same_question|
      @male_user_answer_id = Question.find(same_question).answers.where(user_id: self.male_user_id).first.id
      @female_user_answer_id = Question.find(same_question).answers.where(user_id: self.female_user_id).first.id
      question_ids.push(same_question)
      male_answers_ids.push(@male_user_answer_id)
      female_answers_ids.push(@female_user_answer_id)
    end
    @verify = self.build_verify(question_ids: question_ids, male_answers_ids: male_answers_ids, female_answers_ids: female_answers_ids)
    @verify.save

  end
end
