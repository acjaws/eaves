# == Schema Information
#
# Table name: comments
#
#  id         :integer          not null, primary key
#  verify_id  :integer          not null
#  user_id    :integer          not null
#  score      :integer
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Comment < ApplicationRecord
  validates_uniqueness_of :user_id, scope: :verify_id
  validates_numericality_of :score, only_integer: true, allow_nil: true, great_than_or_equal_to: 0, less_than_or_equal_to: 100
  validates_presence_of :score, :content

  belongs_to :user
  belongs_to :verify, counter_cache: true

  before_save :check_match_status!
  after_save :update_avg_score
  after_update :update_verify_comments_count
  after_save :check_completed_comments

  def comment_completed?
    self.score.present? && self.content.present?
  end

  private

  def update_avg_score
    avg_score = Comment.where(verify_id: self.verify_id).average(:score)
    self.verify.update(avg_score: avg_score)
  end

  def update_verify_comments_count
    self.verify.update(completed_comments_count: self.verify.comments.where.not(score: 0, content: "").size)
  end

  def check_completed_comments
    if self.verify.completed_comments_count == 5
      self.verify.match.change_status_to_expired
    end
  end

  def check_match_status!
    if self.verify.match.status == "ACTIVE"
      if self.verify.comments_count > 5
        self.errors.add(:base, "評論已經滿了")
        throw :abort
      else
        return true
      end
    else
      self.errors.add(:base, "約會尚未開始或已結束")
      throw :abort
    end
  end
end
