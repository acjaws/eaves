class MakeMatch < ApplicationRecord
  def self.start
    # 取出上次留下的queue
    @queue = MatchQueue.last
    if !@queue.nil?
      @male_queue = @queue.male_queue
      @female_queue = @queue.female_queue
    else
      @male_queue = []
      @female_queue = []
    end

    #取出目前User中狀態為MATCH_ALLOW且不在male_queue & female_queue裡面的male, female user
    @match_allow_males = User.where.not(id: @male_queue).where(match_type: 'MATCH_ALLOW', gender: 'Male').pluck(:id)
    @match_allow_females = User.where.not(id: @female_queue).where(match_type: 'MATCH_ALLOW', gender: 'Female').pluck(:id)

    # 將取出的user放進對應的gender queue
    @male_queue += @match_allow_males
    @female_queue += @match_allow_females

    # 判斷是否男和女的queue都有存在value
    if @male_queue.any? && @female_queue.any?
      # 建立 match pairs
      @match_pairs = []
      @male_kickout = []
      @female_kickout = []
      # 從男生列表開始掃描
      @male_queue.each do |male_id|
        @female_queue.each do |female_id|
          # 如果沒有配對過，而且有回答相同的問題超過三題就加入配對清單，從女生待配對列表刪除
          same_questions = User.find_by(id: male_id).answered_questions.ids & User.find_by(id: female_id).answered_questions.ids
          if Match.find_by(male_user_id: male_id, female_user_id: female_id).nil? && same_questions.size >= 3
            @match_pairs.push([male_id, female_id])
            @male_kickout.push(male_id)
            @female_queue.delete(female_id)
            break
          end
        end
      end
      # 刪掉已配對的男生
      @male_queue -= @male_kickout
    end
    # 處理重複配對
    if @male_queue.blank? || @female_queue.blank?
      MatchQueue.create(male_queue: @male_queue, female_queue: @female_queue)
    else
      @male_queue.each do |male_id|
        @female_queue.each do |female_id|
          @this_matches = Match.where(male_user_id: @male_id).where(female_user_id: @female_id)
          if @this_matches.size < 5
            @same_questions = User.find_by(id: male_id).answered_questions.ids & User.find_by(id: female_id).answered_questions.ids
            @has_matched_question = []
            @this_matches.each do |match|
              @has_matched_question = match.verify.question_ids | @has_match_question
            end
            if @same_questions.size - @has_matched_question.size > 3
              @match_pairs.push([male_id, female_id])
              @male_kickout.push(male_id)
              @female_queue.delete(female_id)
              break
            end
          end
        end
      end
      @male_queue -= @male_kickout
      # 把沒配對成功的清單存起來下次使用
      MatchQueue.create(male_queue: @male_queue, female_queue: @female_queue)
    end
    # 開始配對
    @match_pairs.each do |match_pair|
      @match = Match.new(male_user_id: match_pair[0], female_user_id: match_pair[1])
      if @match.save
        puts "#{@match.id}: #{@match.male_user.username} and #{@match.female_user.username}"
      else
        puts @match.errors.full_messages.to_sentence
      end
    end
  end
end