# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  username               :string
#  gender                 :string
#  birthday               :date
#  occupation             :string
#  salary                 :string
#  interest               :string
#  fb_uid                 :string
#  fb_token               :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  sash_id                :integer
#  level                  :integer          default(0)
#  envelope               :integer          default(0)
#  current_questions      :text
#  match_type             :string           default("MATCH_FORBID")
#  role                   :text
#  auto_envelope          :boolean          default(FALSE)
#

class User < ApplicationRecord
  has_merit

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  serialize :current_questions, Array
  has_many :answers, dependent: :destroy
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: [:facebook]
  has_many :answered_questions, through: :answers, source: :question
  has_many :comments
  has_many :verifies, through: :comments

  # 增加user和match的關聯，可找到自己的配對，for male user
  has_many :female_pairings, class_name: "Match", foreign_key: :male_user_id
  # 增加user自關聯，可找到與自己配對成功的對象，for male user
  has_many :matched_female_users, through: :female_parings, source: :female_user
  # 增加user和match的關聯，可找到自己的配對，for female user
  has_many :male_pairings, class_name: "Match", foreign_key: :female_user_id
  # 增加user自關聯，可找到與自己配對成功的對象，for female user
  has_many :matched_male_users, through: :male_parings, source: :male_user

  def self.from_omniauth(auth)
    # Case 1: Find existing user by facebook uid
    user = User.find_by_fb_uid( auth.uid )
    if user
      user.fb_token = auth.credentials.token
      user.save!
      return user
    end

    # Case 2: Find existing user by email
    existing_user = User.find_by_email( auth.info.email )
    if existing_user
      existing_user.fb_uid = auth.uid
      existing_user.fb_token = auth.credentials.token
      existing_user.save!
      return existing_user
    end

    # Case 3: Create new password
    user = User.new
    user.fb_uid = auth.uid
    user.fb_token = auth.credentials.token
    user.email = auth.info.email
    user.password = Devise.friendly_token[0,20]
    user.username = auth.info.name
    user.save!
    return user
  end

  def add_envelope
    self.envelope += 1
    self.save!
    Notification.create(notify_type: 'add_envelope', user: self, content: "#{self.envelope}")
  end

  #自動交換信封: 當信封數量<3且開啟auto_envelope時使用
  def auto_exchange_envelope
    while self.envelope < 3 && self.auto_envelope == true
      if self.exchange_envelope == true
        Notification.create(notify_type: "noti", user: self, content: "信封數量小於三，自動交換信封!")
      else
        break
      end
    end
  end

  #交換信封: 扣分與加信封
  def exchange_envelope
    if self.points >= 200
      self.add_envelope
      self.subtract_points 200
      return true
    else
      return false
    end
  end

  def subtract_envelope
    if self.envelope > 0
      self.envelope -= 1
      self.save!
      self.auto_exchange_envelope
    end
  end

  def basic_info_completed?
    self.gender.present? && self.birthday.present? && self.occupation.present? && self.salary.present? && self.interest.present?
  end

  def join_comment?(match)
    self.comments.pluck(:verify_id).include?(match.verify.id)
  end

  def finish_comment?(match)
    return false if !join_comment?(match)
    self.comments.find_by_verify_id(match.verify.id).comment_completed?
  end

  def has_other_match_unfinished?(match)

    comments = self.comments.where(content: nil).or(self.comments.where(score: nil)).or(self.comments.where(content: ""))
    if comments.present?
      return comments.first.verify_id != match.verify.id
    end
  end

  def admin?
    self.role == "admin"
  end

  def change_type_to_matched
    self.update(match_type: "MATCHED")
  end

  def change_type_to_dating
    self.update(match_type: "DATING")
  end

  def change_type_to_allow
    self.update(match_type: "MATCH_ALLOW")
  end

  def has_budge(id)
    Merit::Badge.find(id).users.include?(self)
  end

  def check_answered_all_question
    #1.完成基本問題的狀態轉換 2.用戶狀態的顯示判斷
    if self.match_type == "MATCH_FORBID"
      if self.answered_questions.where(category: "basic").distinct.pluck(:question_id).sort.length >= Question.where(category: "basic").length
        self.change_type_to_allow
        @status = "ALLOW"
        Notification.create(notify_type: "noti", user: self, content:"完成基本問題，您將開始找尋約會對象!")
      else
        @status = "INITIAL"
      end
    elsif self.envelope == 0 && self.match_type == "MATCH_ALLOW"
      @status = "NO_ENVELOPE"
    elsif self.match_type == "MATCHED"
      @status = "MATCHED"
    elsif self.match_type == "DATING"
      @status = "DATING"
    else
      @status = "ALLOW"
    end
    return @status
  end
  def check_answered_question
    if self.match_type == "MATCH_FORBID" && self.answered_questions.size >= 3
      self.change_type_to_allow
      Notification.create(notify_type: "noti", user: self, content:"已完成三題回答，您將開始找尋約會對象!")
    end
  end
end
