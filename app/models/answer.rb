# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  question_id :integer
#  content     :text
#  weight      :float
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Answer < ApplicationRecord
  validates_presence_of(:content,:user_id,:question_id)
  belongs_to :question, optional: true
  belongs_to :user, optional: true
  validates_uniqueness_of :user_id, scope: :question_id

  after_create :answer_question_get_points

  private
  def answer_question_get_points
    self.user.add_points 5
    Notification.create(notify_type: 'add_points', user: self.user, content: "#{self.user.points}")
  end
end
