# == Schema Information
#
# Table name: verifies
#
#  id                       :integer          not null, primary key
#  match_id                 :integer
#  question_ids             :text
#  male_answers_ids         :text
#  female_answers_ids       :text
#  comments_count           :integer          default(0)
#  avg_score                :integer          default(0)
#  completed_comments_count :integer          default(0)
#

class Verify < ApplicationRecord
  serialize :question_ids, Array
  serialize :male_answers_ids, Array
  serialize :female_answers_ids, Array

  belongs_to :match, optional: true
  has_many :comments
  has_many :users, through: :comments
end
