# == Schema Information
#
# Table name: questions
#
#  id             :integer          not null, primary key
#  title          :string
#  female_content :text
#  male_content   :text
#  category       :string
#  weight         :float
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Question < ApplicationRecord
  validates_presence_of(:title,:female_content,:male_content)
  has_many :answers, dependent: :destroy
  has_many :answered_users, through: :answers, source: :user

  QUESTION_CATEGORY = ["感情觀","金錢觀","家庭觀"]

end
