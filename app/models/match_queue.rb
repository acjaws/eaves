# == Schema Information
#
# Table name: match_queues
#
#  id           :integer          not null, primary key
#  male_queue   :text
#  female_queue :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class MatchQueue < ApplicationRecord
  serialize :male_queue, Array
  serialize :female_queue, Array
end
