# Learn more: http://github.com/javan/whenever
# 功能：設定一個時段之後執行command 或是 rake
# 用法：
#      參考github網址
#      每次更新完後，記得「whenever --set 'environment=development' --update-crontab」，更新schedule.rb
#      然後再執行「whenever」，並在log檔中觀看相關的output訊息


require File.expand_path(File.dirname(__FILE__) + '/environment')
set :output, {:standard => 'log/'+Time.now.strftime('%F')+'-cron_log.log', :error => 'log/'+Time.now.strftime('%F')+'-cron_error_log.log'}
env :PATH, ENV['PATH']

every 15.minutes do

  rake "dev:make_matches", :environment => @environment

end

every 15.minutes do

  rake "fake:comments", :environment => @environment

end

every 1.day, at: '12:10 pm' do
  rake "dev:push_questions", :environment => @environment
end