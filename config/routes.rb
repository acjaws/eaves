Rails.application.routes.draw do
  mount Notifications::Engine => "/notifications"
  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }, :skip => [:sessions]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  as :user do
    get 'user/signin' => 'devise/sessions#new', :as => :new_user_session
    post 'user/signin' => 'devise/sessions#create', :as => :user_session
    get 'user/signout' => 'devise/sessions#destroy', :as => :destroy_user_session
  end

  root "matches#my"

  # about eaves
  resources :about, only: [:index]
  get '/team', :to => 'about#team', :as => :team
  get '/consent', :to => 'about#consent', :as => :consent

  # matches (配對用)
  resources :matches, only: [:show] do
    collection do
      get :my
      get :my_all
      get :help
      get :help_all
    end
    member do
      post :verify
    end
    resources :comments, only: :index
  end

  #page (問題用)
  resources :pages, except: [:edit, :destroy] do
    collection do
      get :me
      get :me_done
      post :answer
    end
  end

  #Email註冊重複的ajax驗證
  post '/checkemail' => 'users#emailcheck', as: :checkemail


  #以下沒有動

  resources :users, except: [:new, :create, :destroy] do
    member do
      post :exchange_envelope
    end
  end

  # simple_message_system
  resources :conversations do
    resources :messages
  end

  get '/getBasicQuestion', :to => 'matches#getBasicQuestion', :as => :getBasicQuestion  
  post '/postBasicQuestion', :to => 'matches#postBasicQuestion', :as => :postBasicQuestion  
  get '/getAdvancedQuestion', :to => 'matches#getAdvancedQuestion', :as => :getAdvancedQuestion  
  get '/getAnswersHistory', :to => 'matches#getAnswersHistory', :as => :getAnswersHistory  
  post '/postAdvancedQuestion', :to => 'matches#postAdvancedQuestion', :as => :postAdvancedQuestion  
  post '/postAutoEnvelope', :to => 'matches#postAutoEnvelope', :as => :postAutoEnvelope  

  # admin routes
  namespace :admin do
    root "admin/users#index"
    resources :users, only: [:index, :show]
    resources :questions
    resources :answers, only: [:index]
    resources :matches, only: [:index, :show]
  end

  require 'sidekiq/web'
  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end
end
