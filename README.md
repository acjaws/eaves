# Eaves

It's weired that there're so many online dating services, but we still single, so as freinds around us.
That's where refelects limitations of existing dating services, failed to link values of individuals.

Eaves tries to match people who share close values.
System will help you match to someone may shared same values like you.
And meanwhile, you have to help others match, to see if there are on the same page.
Whole process will like you have some virtaul datings with your values and views on things.
As the cycle goes, we believe that everyone can find someone truely suit themself eventually.

#### [Implementation Proposal in Chinese (link)](https://drive.google.com/drive/u/0/folders/1gQAYg7xshcPrq8z97M2qznaeljd7bMQi)

## Installation
---
This project is available on Ruby 2.4.1 and Rails 5.1.6
You can install this project with few steps:

## Clone repository
Clone the source code from bitbucket to your computer.

## Generate configuration files
Generate a secret key into `/config/secrets.yml`

```
$ cd eaves
$ echo -e "development:\n secret_key_base: `rails secret`" | tee config/secrets.yml
```

Then create a `database.yml` under your /config folder

/config/database.yml
```
default: &default
adapter: sqlite3
pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
timeout: 5000

development:
<<: *default
database: db/development.sqlite3
pool: 25

```

## Setup database
Setup your database
```
$ bundle exec rails db:setup
```

Setup admin account

```
$ bundle exec rails db:seed

```

## Setup facebook omniauth

add new file facebook.yml under your /config folder

```
development:
  app_id: your_facebook_app_id
  secret: your_facebook_app_serect_key
  callback_url: your omniauth facebook callback url
```

## Setup Your own question
edit base_questions.rake under your /lib/task folder
```
BASIC_QUESTION = [
...
]
```
you can setting your own question in BASIC_QUESTION

then

```
$ bundle exec rails fake:basic_questions

```
## Run your redis and sidekiq
This application user sidekiq and redis to handle ActiveJob
ensure that redis already installed
```
$ brew install redis
$ redis-server /usr/local/etc/redis.conf
```
then sidekiq

```
$ bundle exec sidekiq
```

## Update crontab for match start
edit schedule.rb under your /config folder to setting whenever it make the match start

default setting is every 10 minutes to make matches
```
every 10.minutes do
  rake "dev:make_matches", :environment => @environment
end
```
/config/schedule.rb


then update your setting
```
$ whenever --set 'environment=development' --update-crontab
```

## Run your application
```
$ bundle exec rails s
```

Enjoy to make a Match



#### by JAWS
