# == Schema Information
#
# Table name: matches
#
#  id              :integer          not null, primary key
#  male_user_id    :integer
#  female_user_id  :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  dating_datetime :datetime
#  status          :string           default("ACTIVE")
#  twice           :boolean          default(FALSE)
#

require "rails_helper"

RSpec.describe Match, :type => :model do
  let(:user) { create(:user, email: FFaker::Internet.email) }
  let(:male_user) { create(:user, email: FFaker::Internet.email, gender: "Male") }
  let(:female_user) { create(:user, email: FFaker::Internet.email, gender: "Female") }
  let(:match) { create(:match, male_user: male_user, female_user: female_user) }
  let(:match_active) { create(:match, male_user: male_user, female_user: female_user, status: "ACTIVE") }
  let(:match_active2) { create(:match, male_user: male_user, female_user: female_user, status: "ACTIVE") }
  let(:match_expired) { create(:match, male_user: male_user, female_user: female_user, status: "EXPIRED") }
  let(:verify1) { create(:verify, match: match_active) }
  let(:comment1) { create(:comment, verify: verify1, user: user) }
  let(:verify2) { create(:verify, match: match_active2) }
  let(:comment2) { create(:comment, verify: verify2, user: user, score: 0) }
  let(:comment3) { create(:comment, verify: verify1, user: user, score: 0) }

  describe "配對狀態" do
    it "初始狀態須為約會中" do
      expect(match.status).to eq("ACTIVE")
    end
  end
  describe "#change_status_to_dating" do
    describe "配對狀態" do
      it "結束不可改變為約會" do
        match_expired.change_status_to_dating
        expect(match_expired.status).to eq("EXPIRED")
      end
      describe "使用者" do
        it "配對狀態改變為約會時，男方狀態改為約會中" do
          match
          expect(male_user.match_type).to eq("DATING")
        end
        it "配對狀態改變為約會時，女方狀態改為約會中" do
          match
          expect(female_user.match_type).to eq("DATING")
        end
      end
    end
  end

  describe "#change_status_to_expired"do
    describe "配對狀態" do
      before do
        match_active
      end
      it "可改變為結束" do
        match_active.change_status_to_expired
        expect(match_active.status).to eq("EXPIRED")
      end
      context "第一次約會" do
        before do
          match_active
          verify1
        end
        describe "分數及格" do
          before do
            comment1
          end
          it "再次約會" do
            match_active.change_status_to_expired
            expect(Match.last.male_user).to eq(male_user)
            expect(Match.last.female_user).to eq(female_user)
            expect(Match.last.twice).to be true
          end
        end
        describe "分數不及格" do
          before do
            comment3
          end
          it "不會再次約會" do
            match_count = Match.all.count
            match_active.change_status_to_expired
            match_count_after = Match.all.count
            expect(match_count).to eq(match_count_after)
          end
        end

        describe "無人評論" do
          it "再次約會" do
            match_active.change_status_to_expired
            expect(Match.last.male_user).to eq(male_user)
            expect(Match.last.female_user).to eq(female_user)
            expect(Match.last.twice).to be false
          end
        end
      end
      context "第二次約會" do
        before do
          match_active.twice = true
          verify1
        end
        describe "分數及格" do
          before do
            comment1
          end
          it "第二次獲得徽章" do
              match_active.change_status_to_expired
              expect(male_user.has_budge(1)).to be true
              expect(female_user.has_budge(1)).to be true
          end
          it "第二次不會再次約會" do
              match_count = Match.all.count
              match_active.change_status_to_expired
              match_count_after = Match.all.count
              expect(match_count).to eq(match_count_after)
          end
        end
        describe "無人評論" do
          it "再次約會" do
            match_active.change_status_to_expired
            expect(Match.last.male_user).to eq(male_user)
            expect(Match.last.female_user).to eq(female_user)
            expect(Match.last.twice).to be true
          end
        end
      end
    end

    describe "使用者" do
      before do
        match_active
        verify1
        comment1
        match_active.twice = true
      end
      it "配對狀態改變為結束時，男方狀態改為結束約會" do
        match_active.change_status_to_expired
        expect(male_user.match_type).to eq("MATCH_ALLOW")
      end
      it "配對狀態改變為約會時，女方狀態改為約結束約會" do
        match_active.change_status_to_expired
        expect(female_user.match_type).to eq("MATCH_ALLOW")
      end
    end
  end
end
