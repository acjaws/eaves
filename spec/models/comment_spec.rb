# == Schema Information
#
# Table name: comments
#
#  id         :integer          not null, primary key
#  verify_id  :integer          not null
#  user_id    :integer          not null
#  score      :integer
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require "rails_helper"

RSpec.describe Match, :type => :model do
  let(:user) { create(:user) }
  let(:male_user) { create(:user, email: FFaker::Internet.email, gender: "Male") }
  let(:female_user) { create(:user, email: FFaker::Internet.email, gender: "Female") }
  let(:match) { create(:match, male_user: male_user, female_user: female_user, status: "ACTIVE") }
  let(:verify) { create(:verify, match: match) }
  let(:comment) { create(:comment, verify: verify, user: user) }
  context "#comment_completed?" do
    before do
      match
      verify
      comment
    end
    it "comment with score and content should return true" do
      expect(comment.comment_completed?).to be true
    end
  end
end
