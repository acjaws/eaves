# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  username               :string
#  gender                 :string
#  birthday               :date
#  occupation             :string
#  salary                 :string
#  interest               :string
#  fb_uid                 :string
#  fb_token               :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  sash_id                :integer
#  level                  :integer          default(0)
#  envelope               :integer          default(0)
#  current_questions      :text
#  match_type             :string           default("MATCH_FORBID")
#  role                   :text
#  auto_envelope          :boolean          default(FALSE)
#

require "rails_helper"

RSpec.describe User, :type => :model do
  let(:user) { create(:user) }
  let(:admin) { create(:admin, email: FFaker::Internet.email) }
  let(:male_user) { create(:user, email: FFaker::Internet.email, gender: "Male") }
  let(:female_user) { create(:user, email: FFaker::Internet.email, gender: "Female") }
  let(:user_complete_basic_info) { create(:user_complete_basic_info) }
  let(:user1) { create(:user, username: "1", email: FFaker::Internet.email) }
  let(:user2) { create(:user, username: "2", email: FFaker::Internet.email) }
  let(:user3) { create(:user, username: "3") }
  let(:user4) { create(:user, username: "4") }

  describe "#admin?" do
    it "check admin" do
      expect(admin.admin?).to eq(true)
      expect(user.admin?).to eq(false)
    end
  end

  describe "#add_envelope" do
    it "花200點換一個信封" do
      user.add_envelope
      expect(user.envelope).to eq(1)
    end
  end

  describe "#subtract_envelope" do
    it "沒信封無法減少信封" do
      user.subtract_envelope
      expect(user.envelope).to eq(0)
    end

    it "有信封可以減少信封" do
      user.envelope = 1
      user.subtract_envelope
      expect(user.envelope).to eq(0)
    end
  end

  describe "#basic_info_completed?" do
    it "user has complete basic info" do
      expect(user_complete_basic_info.basic_info_completed?).to be_truthy
    end
  end
end
