require 'rails_helper'

RSpec.describe MakeMatch, type: :model do
  let(:male_user1) { create(:user, email: FFaker::Internet.email, gender: "Male") }
  let(:male_user2) { create(:user, email: FFaker::Internet.email, gender: "Male") }
  let(:male_user3) { create(:user, email: FFaker::Internet.email, gender: "Male") }
  let(:female_user1) { create(:user, email: FFaker::Internet.email, gender: "Female") }
  let(:female_user2) { create(:user, email: FFaker::Internet.email, gender: "Female") }
  let(:female_user3) { create(:user, email: FFaker::Internet.email, gender: "Female") }
  let!(:question1) { create(:question) }
  let!(:question2) { create(:question) }
  let!(:question3) { create(:question) }
  let!(:question4) { create(:question) }
  let!(:question5) { create(:question) }
  describe "start" do
    describe "一男三女配對" do
      before do
        male_user1
        male_user2
        male_user3
        female_user1
        female_user2
        female_user3
        male_user1.change_type_to_allow
        female_user1.change_type_to_allow
        female_user2.change_type_to_allow
        female_user3.change_type_to_allow
      end
      describe "答題都相同" do
        before do
          male_user1.answers.create(question: question1, content: "content")
          male_user1.answers.create(question: question2, content: "content")
          male_user1.answers.create(question: question3, content: "content")
          male_user2.answers.create(question: question1, content: "content")
          male_user2.answers.create(question: question2, content: "content")
          male_user2.answers.create(question: question3, content: "content")
          female_user1.answers.create(question: question1, content: "content")
          female_user1.answers.create(question: question2, content: "content")
          female_user1.answers.create(question: question3, content: "content")
          female_user2.answers.create(question: question1, content: "content")
          female_user2.answers.create(question: question2, content: "content")
          female_user2.answers.create(question: question3, content: "content")
          female_user3.answers.create(question: question1, content: "content")
          female_user3.answers.create(question: question2, content: "content")
          female_user3.answers.create(question: question3, content: "content")
        end
        it "男女都沒配對過" do
          MakeMatch.start
          expect(Match.last.male_user_id).to eq male_user1.id
          expect(Match.last.female_user_id).to eq female_user1.id
          expect(MatchQueue.last.male_queue.size).to eq 0
          expect(MatchQueue.last.female_queue).to eq [5,6]
        end
        it "男一和女一配對過" do
          Match.create(male_user_id: 1, female_user_id: 4, status: "EXPIRED")
          MakeMatch.start
          expect(Match.last.male_user_id).to eq male_user1.id
          expect(Match.last.female_user_id).to eq female_user2.id
          expect(MatchQueue.last.male_queue.size).to eq 0
          expect(MatchQueue.last.female_queue).to eq [4,6]
        end
        it "男一和女一配對過，第二次配對前男二加入配對" do
          MakeMatch.start
          male_user2.change_type_to_allow
          MakeMatch.start
          expect(Match.last.male_user_id).to eq male_user2.id
          expect(Match.last.female_user_id).to eq female_user2.id
          expect(MatchQueue.last.male_queue.size).to eq 0
          expect(MatchQueue.last.female_queue).to eq [6]
        end
      end
      describe "答題不同" do
        it "男一與女二答題相同，男二與女三答題相同" do
          male_user2.change_type_to_allow
          male_user1.answers.create(question: question1, content: "content")
          male_user1.answers.create(question: question2, content: "content")
          male_user1.answers.create(question: question3, content: "content")
          male_user2.answers.create(question: question3, content: "content")
          male_user2.answers.create(question: question4, content: "content")
          male_user2.answers.create(question: question5, content: "content")
          female_user1.answers.create(question: question1, content: "content")
          female_user1.answers.create(question: question4, content: "content")
          female_user1.answers.create(question: question5, content: "content")
          female_user2.answers.create(question: question1, content: "content")
          female_user2.answers.create(question: question2, content: "content")
          female_user2.answers.create(question: question3, content: "content")
          female_user3.answers.create(question: question3, content: "content")
          female_user3.answers.create(question: question4, content: "content")
          female_user3.answers.create(question: question5, content: "content")
          MakeMatch.start
          expect(Match.last.male_user_id).to eq male_user2.id
          expect(Match.last.female_user_id).to eq female_user3.id
          expect(MatchQueue.last.male_queue.size).to eq 0
          expect(MatchQueue.last.female_queue).to eq [4]
        end
      end
      it "只答一題不配對" do
        male_user1.answers.create(question: question1, content: "content")
        female_user1.answers.create(question: question1, content: "content")
        female_user2.answers.create(question: question1, content: "content")
        female_user3.answers.create(question: question1, content: "content")
        MakeMatch.start
        expect(Match.all.size).to eq 0
        expect(MatchQueue.last.male_queue.size).to eq 1
        expect(MatchQueue.last.female_queue).to eq [4,5,6]
      end
    end
  end
end
