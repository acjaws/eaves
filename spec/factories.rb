FactoryBot.define do
  sequence :email do |n|
    "person#{n}@example.com"
  end
  factory :user do
    email
    password "password"

    factory :user_complete_basic_info do
      username FFaker::Name.html_safe_last_name
      gender FFaker::Gender.sample.capitalize
      birthday "1988-01-01"
      occupation FFaker::Job.title
      salary 1000000
      interest FFaker::Lorem.word
    end

    factory :admin do
      role "admin"
    end
  end

  factory :match do
    dating_datetime { Time.now + 2.hours }
  end

  factory :question do
    title "Title"
    male_content "Content"
    female_content "Content"
    category "Category"
  end

  factory :answer do
    user
    question
    content "Content"
  end


  factory :verify do
    match
    avg_score 0
  end

  factory :comment do
    user
    verify
    score 100
    content "content"
  end
end