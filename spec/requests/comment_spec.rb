RSpec.describe 'Comment', type: :request do
  let(:user1) { create(:user) }
  let(:user2) { create(:user, email: FFaker::Internet.email, gender: "Male") }
  let(:user3) { create(:user, email: FFaker::Internet.email, gender: "Female") }
  let(:match) { create(:match, male_user: user2, female_user: user3, status: "ACTIVE") }
  let(:verify) { create(:verify, match: match) }
  let(:comment) { create(:comment, verify: verify, user: user1) }
  describe "/matches/:match_id/comments #index" do
    before do
      user1
      user2
      user3
      match
      verify
      comment
    end
    context "match active by user2 & user3" do
      describe "sign_in as user1" do
        before do
          sign_in(user1)
          get match_comments_path(match)
        end

        it 'should render index' do
          expect(response).to render_template(:index)
        end
      end

      describe "sign_in as user2" do
        before do
          sign_in(user2)
          get match_comments_path(match)
        end

        it "redirect to matches_path" do
          expect(response).to redirect_to(my_matches_path)
        end
      end
    end
    context "match expired by user2 & user3" do
      before do
        match.change_status_to_expired
      end
      describe "sign_in as user2" do
        before do
          sign_in(user2)
          get match_comments_path(match)
        end

        it 'should render index' do
          expect(response).to render_template(:index)
        end
      end
    end
  end
end