RSpec.describe 'Match', type: :request do
  let(:user) { create(:user_complete_basic_info, email: FFaker::Internet.email) }
  let(:user1) { create(:user_complete_basic_info, email: FFaker::Internet.email) }
  let(:user2) { create(:user_complete_basic_info, email: FFaker::Internet.email) }
  let(:user3) { create(:user_complete_basic_info, email: FFaker::Internet.email) }
  let(:user4) { create(:user_complete_basic_info, email: FFaker::Internet.email) }
  let(:user5) { create(:user_complete_basic_info, email: FFaker::Internet.email) }
  let(:male_user) { create(:user_complete_basic_info, email: FFaker::Internet.email, gender: "Male") }
  let(:female_user) { create(:user_complete_basic_info, email: FFaker::Internet.email, gender: "Female") }
  let(:match) { create(:match, male_user: male_user, female_user: female_user) }
  let(:match_active1) { create(:match, male_user: male_user, female_user: female_user, status: "ACTIVE") }
  let(:match_active2) { create(:match, male_user: male_user, female_user: female_user, status: "ACTIVE") }
  let(:match_active3) { create(:match, male_user: male_user, female_user: female_user, status: "ACTIVE") }
  let(:match_active4) { create(:match, male_user: male_user, female_user: female_user, status: "ACTIVE") }
  let(:match_active5) { create(:match, male_user: male_user, female_user: female_user, status: "ACTIVE") }
  let(:match_active6) { create(:match, male_user: male_user, female_user: female_user, status: "ACTIVE") }
  let(:verify1) { create(:verify, match: match_active1) }
  let(:comment) { create(:comment, verify: verify1, user: user, score: 0) }
  let(:verify2) { create(:verify, match: match_active2) }
  let(:comment2) { create(:comment, verify: verify2, user: user, score: 0) }
  let(:verify3) { create(:verify, match: match_active3) }
  let(:comment3) { create(:comment, verify: verify3, user: user, score: 0) }
  let(:verify4) { create(:verify, match: match_active4) }
  let(:comment4) { create(:comment, verify: verify4, user: user, score: 0) }
  let(:verify5) { create(:verify, match: match_active5) }
  let(:comment5) { create(:comment, verify: verify5, user: user, score: 0) }
  let(:verify6) { create(:verify, match: match_active6) }
  let(:comment6) { create(:comment, verify: verify6, user: user, score: 0) }
  context "#my" do
    describe 'user not login' do
      it 'will redirect to log in page' do
        get my_matches_path
        expect(response).to redirect_to(new_user_session_path)
        expect(response).to have_http_status(302)
      end
    end

    context 'user log in' do
      before do
        user
        sign_in(user)
        get my_matches_path
      end

      it 'can render my' do
        expect(response).to render_template(:my)
      end
    end
  end

  context "#show" do
    before do
      match
    end
    it "配對狀態不是約會中無法加入填寫評論" do
      match.change_status_to_expired
      user
      sign_in(user)
      get match_path(match)
      expect(response).to redirect_to(root_path)
    end
    describe "配對狀態是約會中" do
      before do
        match_active1
        @verify = match_active1.create_verify(avg_score: 0)
        Comment.create(verify: @verify, user: user1, score: 60, content: "content")
        Comment.create(verify: @verify, user: user2, score: 60, content: "content")
        Comment.create(verify: @verify, user: user3, score: 60, content: "content")
        Comment.create(verify: @verify, user: user4, score: 60, content: "content")
      end
      describe "user sign in" do
        before do
          user
          sign_in(user)
        end
        it "驗證參加人數等於五人無法加入驗證" do
          Comment.create(verify: @verify, user: user5, score: 60, content: "content")
          get match_path(match_active1)
          expect(response).to redirect_to(help_matches_path)
        end
        it "驗證參加人數等於五人但已加入過則可加入驗證" do
          Comment.create(verify: @verify, user: user, score: 60, content: "content")
          get match_path(match_active1)
          expect(response).to render_template(:show)
        end
        describe "配對驗證參加人數少於五人" do
          it "使用者加入的驗證少於五個可以加入驗證" do
            match_active2
            verify2
            comment2
            match_active3
            verify3
            comment3
            match_active4
            verify4
            comment4
            match_active5
            verify5
            comment5
            get match_path(match_active1)
            expect(response).to render_template(:show)
          end
          it "使用者加入的驗證大於五個無法加入驗證" do
            match_active2
            verify2
            comment2
            match_active3
            verify3
            comment3
            match_active4
            verify4
            comment4
            match_active5
            verify5
            comment5
            match_active6
            verify6
            comment6
            get match_path(match_active1)
            expect(response).to redirect_to(help_matches_path)
          end
        end
      end
      describe "male_user sign in" do
        before do
          male_user
          sign_in(male_user)
        end
        it "redirect_to my_matches_path" do
          get match_path(match_active1)
          expect(response).to redirect_to(root_path)
        end
      end
    end
  end

  context "#verify" do
    before do
      @match = Match.create(male_user: male_user, female_user: female_user, status: "ACTIVE", dating_datetime: Time.now.to_datetime)
      Comment.create(verify: @match.verify, user: user1, score: 60, content: "content")
      Comment.create(verify: @match.verify, user: user2, score: 60, content: "content")
      Comment.create(verify: @match.verify, user: user3, score: 60, content: "content")
      Comment.create(verify: @match.verify, user: user4, score: 60, content: "content")
      Comment.create(verify: @match.verify, user: user, score: 60, content: "content")
      user
      sign_in(user)
    end

    describe 'when successfully save' do
      before do
        post verify_match_path(@match), params: { comment: { socre: 50, content: "content" } }
      end
      it 'will create comment on that match verify' do
        expect(@match.verify.comments_count).to eq 5
      end

      it 'will create current users reply' do
        expect(Comment.last.user).to eq user
      end
    end
  end
end