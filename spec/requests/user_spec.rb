RSpec.describe 'User', type: :request do
  let(:user) { create(:user) }
  let(:user_complete_basic_info) { create(:user_complete_basic_info, email: FFaker::Internet.email) }
  context "GET #index" do
    before do
      user
      sign_in(user)
    end

    describe "基本資料" do
      it "未填寫完會跳轉users#edit" do
        get root_path
        expect(response).to redirect_to(edit_user_path(user))
      end

      it "填寫完才可打開首頁" do
        user_complete_basic_info
        sign_in(user_complete_basic_info)
        get root_path
        expect(response).to render_template(root_path)
      end
    end
  end

  context "GET #show" do
    before do
      user
      user_complete_basic_info
      sign_in(user_complete_basic_info)
    end
    describe "go to current_user profile page" do
      it "will show current_user profile" do
        get user_path(user_complete_basic_info)
        expect(response).to be_success
        expect(response).to render_template("show")
        expect(assigns[:user]).to eq(user_complete_basic_info)
      end
    end
    describe "go to other_user profile page" do
      it "will show current_user profile" do
        get user_path(user)
        expect(response).to redirect_to(user_path(controller.current_user))
      end
    end
  end

  context "#edit" do
    before do
      user
      user_complete_basic_info
      sign_in(user)
    end

    describe 'go to edit page' do
      it 'will render edit page' do
        get edit_user_path(user)
        expect(response).to render_template(:edit)
      end

      it 'will redirect if not this user' do
        get edit_user_path(user_complete_basic_info)
        expect(response).to redirect_to(edit_user_path(controller.current_user))
      end
    end
  end

  context "#update" do
    before do
      user
      sign_in(user)
    end

    describe 'successfully update' do
      it 'will change users profile' do
        expect(user.username).to be nil
        patch "/users/#{user.id}", params: {
          user: {
            username: "username",
            gender: "Male",
            birthday: "2000-01-01",
            occupation: "occupation",
            salary: "50",
            interest: "interest"
          }
        }
        user.reload
        expect(user.username).to_not be_nil
        expect(user.username.length).not_to be_zero
      end
    end
  end

  context "#exchange_envelope" do
    describe "" do

    end
  end

end