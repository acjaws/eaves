namespace :fake do

  task users20: :environment do
    20.times do |i|
      user_name = FFaker::Name.first_name
      User.create(
        username: "#{user_name}",
        email: "#{user_name}@com",
        password: "12345678",
        gender: ["Male","Female"].sample,
        birthday: FFaker::Time.between(30.years.ago, 18.years.ago),
        occupation: FFaker::Job.title,
        salary: "#{rand(300..1000)}K",
        interest: FFaker::Sport.name,
      )
      puts "create #{user_name}"
    end
    User.all.each do |user|
      Question.all.each do |question|
        user.answers.create(question: question, content: FFaker::Lorem.sentence)
      end
      user.check_answered_all_question
      puts "#{user.username} answered all question"
    end
  end

  task basics20: :environment do
    20.times do |i|
      Question.create(
        title: FFaker::Lorem.word,
        female_content: FFaker::Lorem.sentence,
        male_content: FFaker::Lorem.sentence,
        category: "basic"
      )
    end
    puts "create 20 fake basic question"
  end

  task answers: :environment do
    1000.times do |i|
      Answer.create(
        user: User.all.sample,
        question: Question.all.sample,
        content: FFaker::Lorem.sentence
      )
    end
    puts "create random 1000 fake answers"
  end

  task comments: :environment do
    FAKE_COMMENTS = [
      { score:50, content: "普普通通" },
      { score:100, content: "天造地設的一對" },
      { score:90, content: "很速配喔" },
      { score:85, content: "還不賴滿適合的" },
      { score:70, content: "可以喔，在一起" },
      { score:0, content: "會吵架吵不停吧" },
      { score:87, content: "87分不能再高了" },
      { score:20, content: "我不看好" },
      { score:30, content: "無言" },
      { score:40, content: "不太適合" },
      { score:56, content: "就不要勉強了吧" }
    ]
    @users = User.all.sample(5)
    @users.each do |user|
      comment = FAKE_COMMENTS.sample
      @match = Match.where(status: "ACTIVE").joins(:verify).where("comments_count < 5").sample
      user.comments.create(
                                          verify: @match.verify,
                                          score: comment[:score],
                                          content: comment[:content]
                                        )
      puts "#{user.username} 評論了#{@match.male_user.username}與#{@match.female_user.username}的約會"
    end
  end

  task initialize_users_match: :environment do
    User.all.update(match_type: "MATCH_ALLOW")
    Match.destroy_all
    Comment.destroy_all
    Verify.destroy_all
    Rake::Task['dev:make_matches'].execute
  end
end