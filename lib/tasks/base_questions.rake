namespace :fake do
  task basic_questions: :environment do
    BASIC_QUESTION = [
      { title: "假日", male_content: "假日喜歡做些什麼活動？如果對方跟你的興趣不同怎麼辦？", female_content: "假日喜歡做些什麼活動？如果對方跟你的興趣不同怎麼辦？", category: Question::QUESTION_CATEGORY[0] },
      { title: "適婚年齡", male_content: "你現在幾歲？有沒有規劃什麼年紀結婚呢？有什麼考量？", female_content: "你現在幾歲？有沒有規劃什麼年紀結婚呢？有什麼考量？", category: Question::QUESTION_CATEGORY[2] },
      { title: "巧遇前女友", male_content: "你跟現任女友逛街遇到前女友，你的反應是什麼？打招呼或寒暄嗎？", female_content: "你跟現任男友逛街遇到前男友，你的反應是什麼？打招呼或寒暄嗎？", category: Question::QUESTION_CATEGORY[0] },
      { title: "第一次約會費用", male_content: "跟女生第一次約會，你會怎麼處理吃飯或看電影的帳單？這樣做的原因？", female_content: "跟男生第一次約會，你會怎麼處理吃飯或看電影的帳單？這樣做的原因？", category: Question::QUESTION_CATEGORY[1] },
      { title: "男生告白", male_content: "你是怎麼決定要不要跟女生告白？通常認為怎麼樣告白比較好？", female_content: "若有男生向你告白，在你想像中，在什麼狀況下告白比較自然？", category: Question::QUESTION_CATEGORY[1] },
      { title: "未婚同居", male_content: "未婚前男女應該同居嗎？你對於未婚同居這件事的看法是？", female_content: "未婚前男女應該同居嗎？你對於未婚同居這件事的看法是？", category: Question::QUESTION_CATEGORY[2] },
      { title: "個性急緩", male_content: "你認為你個性急嗎？急驚風或慢郎中，請簡單舉個例子。", female_content: "你比較喜歡急驚風或慢郎中個性的男生？為什麼？", category: Question::QUESTION_CATEGORY[0] },
      { title: "情侶約會費用", male_content: "情侶交往時約會的開銷，你認為怎麼樣分攤比較好？", female_content: "情侶交往時約會的開銷，你認為怎麼樣分攤比較好？", category: Question::QUESTION_CATEGORY[2] },
      { title: "女生已讀不回", male_content: "若你的女朋友常對你已讀不回，你會怎麼處理？", female_content: "若你的男朋友常對你已讀不回，你會怎麼處理？", category: Question::QUESTION_CATEGORY[0] }
    ]
    BASIC_QUESTION.each do |question|
      Question.create(
        title: question[:title],
        male_content: question[:male_content],
        female_content: question[:female_content],
        category: question[:category]
      )
    end
    puts "added basic questions"
  end
end