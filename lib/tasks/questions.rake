namespace :dev do

  task push_questions: :environment do
    @users = User.where.not(match_type:"MATCH_FORBID")
    @users.each do |user|
      if user.current_questions.empty?
        notyet_responsed_questions = [] #儲存已回答過的題目id
        user.answered_questions.where(category: Question::QUESTION_CATEGORY).each do |q|
          notyet_responsed_questions << q.id
        end
        @questions = Question.where.not(id: notyet_responsed_questions).sample(3)
        #排除已回答過的問題，從中sample三個題目
        @questions.each do |q|
          user.current_questions << q.id
          user.save
        end
        puts "#{user.username} << #{user.current_questions}"
      end
    end
  end

end